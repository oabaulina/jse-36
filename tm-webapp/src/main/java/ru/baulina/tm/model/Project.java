package ru.baulina.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@XmlRootElement
@Table(name="project")
public class Project extends AbstractModel implements Serializable {

    @NotNull
    @Column(nullable = false)
    private String name;

    @Nullable
    private String description;

    @NotNull
    @ManyToOne
    private User user;

    public Project() {
    }

    public Project(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
