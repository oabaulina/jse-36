package ru.baulina.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;

@Component
public class DataXmlSaveListener extends AbstractDataListener {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@dataXmlSaveListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA XML SAVE]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataXmlSave(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
