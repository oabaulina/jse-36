<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/css/style-sing-in.css" rel="stylesheet">
    </head>
    <body>

        <form class="modal-content" action="/register" method="post">
            <div class="container">
                <h1>Sign Up</h1>
                <p>Please fill in this form to create an account.</p>
                <hr>
                    <label for="login"><b>Login</b></label>
                    <input type="text" placeholder="Enter login" name="login" required>

                    <label for="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="psw" required>


                <div class="clearfix">
                    <button class="cancelbtn"><a href="/">Cancel</a></button>
                    <button type="submit" class="signupbtn">Sign Up</button>
                </div>
            </div>
        </form>

    </body>
</html>