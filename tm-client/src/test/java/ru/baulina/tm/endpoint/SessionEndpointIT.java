package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.IntegrationTest;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationTest.class)
public class SessionEndpointIT {

//    @Rule
//    public final TestName testName = new TestName();
//
//    @NotNull
//    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
//    @NotNull
//    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
//
//    @Before
//    public void setTestName() throws Exception {
//        System.out.println(testName.getMethodName());
//        @Nullable final SessionDTO session = sessionEndpoint.openSession("test", "test");
//        sessionEndpoint.closeAllSession(session);
//    }
//
//    @Test
//    public void testOpenSession() {
//        @NotNull final SessionDTO session = sessionEndpoint.openSession("test", "test");
//        Assert.assertEquals(1, sessionEndpoint.getListSession(session).size());
//        @NotNull String segnExpected = session.getSignature();
//        @NotNull Session actuslSession = sessionEndpoint.getListSession(session).get(0);
//        @NotNull String segnActual = actuslSession.getSignature();
//        Assert.assertEquals(segnExpected, segnActual);
//    }
//
//    @Test
//    public void testGetListSession() {
//        @NotNull final SessionDTO session1 = sessionEndpoint.openSession("test", "test");
//        @NotNull final SessionDTO session2 = sessionEndpoint.openSession("test", "test");
//        @Nullable final List<String> expected = new ArrayList<>();
//        expected.add(session1.getSignature());
//        expected.add(session2.getSignature());
//        @NotNull final List<Session> sessions = sessionEndpoint.getListSession(session1);
//        @NotNull final List<String> actual = new ArrayList<>();
//        for (@NotNull final Session session: sessions) {
//            actual.add(session.getSignature());
//        }
//        Assert.assertEquals(expected, actual);
//    }
//
//    @Test (expected = Exception.class)
//    public void testCloseAllSession() {
//        @Nullable final SessionDTO session = sessionEndpoint.openSession("test", "test");
//        @Nullable final SessionDTO session1 = sessionEndpoint.openSession("test", "test");
//        @Nullable final SessionDTO session2 = sessionEndpoint.openSession("test", "test");
//        Assert.assertNotNull(sessionEndpoint.getUser(session));
//        Assert.assertNotNull(sessionEndpoint.getUser(session1));
//        Assert.assertNotNull(sessionEndpoint.getUser(session2));
//        sessionEndpoint.closeAllSession(session);
//        Assert.assertNull(sessionEndpoint.getUser(session));
//        Assert.assertNull(sessionEndpoint.getUser(session1));
//        Assert.assertNull(sessionEndpoint.getUser(session2));
//    }
//
//    @Test (expected = Exception.class)
//    public void testCloseSession() {
//        @Nullable final SessionDTO sessionTest = sessionEndpoint.openSession("test", "test");
//        Assert.assertNotNull(sessionTest);
//        sessionEndpoint.closeSession(sessionTest);
//        Assert.assertEquals(0, sessionEndpoint.getListSession(sessionTest).size());
//    }
//
//    @Test
//    public void testGetUser() {
//        @Nullable final SessionDTO session = sessionEndpoint.openSession("test", "test");
//        Assert.assertEquals("test", sessionEndpoint.getUser(session).getLogin());
//        Assert.assertNotNull(sessionEndpoint.getUser(session));
//    }

}