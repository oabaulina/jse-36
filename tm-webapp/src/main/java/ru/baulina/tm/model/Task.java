package ru.baulina.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@XmlRootElement
@Table(name="task")
public class Task extends AbstractModel implements Serializable {

    @NotNull
    @Column(nullable = false)
    private String name;

    @Nullable
    private String description;

    @NotNull
    @ManyToOne
    private User user;

    public Task() {
    }

    public Task(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
