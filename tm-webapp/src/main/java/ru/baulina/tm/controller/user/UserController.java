package ru.baulina.tm.controller.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.model.User;

@Controller
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/user/create")
    public String create() {
        userService.create("New User " + System.currentTimeMillis(), "newPassword");
        return "redirect:/users";
    }

    @GetMapping("/user/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        final User user = userService.findById(id);
        return new ModelAndView("user/user-edit", "user", user);
    }

    @PostMapping("/user/edit")
    public String edit(@ModelAttribute("user") User user) {
        final Long id = user.getId();
        final String email = user.getEmail();
        final String firstName = user.getFirstName();
        final String LastName = user.getLastName();
        userService.changeUser(email, firstName, LastName, id);
        return "redirect:/users";
    }

    @GetMapping("/user/delete/{id}")
    public String delete(
            @PathVariable("id") Long id
    ) {
        userService.removeById(id);
        return "redirect:/users";
    }

}
