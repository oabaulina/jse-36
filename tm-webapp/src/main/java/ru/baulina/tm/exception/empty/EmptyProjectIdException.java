package ru.baulina.tm.exception.empty;

public class EmptyProjectIdException  extends RuntimeException{

    public EmptyProjectIdException() {
        super("Error! ProjectId is empty...");
    }

}
