package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectDTO;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIdListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectUpdateByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT]");
        @Nullable final Long id;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER ID:");
            id = TerminalUtil.nexLong();
        }
        @Nullable final SessionDTO session = getSession();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProjectById(
                session, session.getUserId()
        );
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        @Nullable final String name;
        @Nullable final String description;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER NAME:");
            name = TerminalUtil.nextLine();
            System.out.println("ENTER DESCRIPTION:");
            description = TerminalUtil.nextLine();
        }
        projectEndpoint.updateProjectById(session, id, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
