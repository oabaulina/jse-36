package ru.baulina.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.exception.user.AccessDeniedException;

@Component
public class DataBinarySaveListener extends AbstractDataListener {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@dataBinarySaveListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA BINARY SAVE]");
        @Nullable final SessionDTO session = getSession();
        if (session == null) throw new AccessDeniedException();
        adminDumpEndpoint.dataBinarySave(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
