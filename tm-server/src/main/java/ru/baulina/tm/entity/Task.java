package ru.baulina.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@XmlRootElement
@NoArgsConstructor
@Table(name="task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractEntity implements Serializable {

    @NotNull
    @Column(nullable = false)
    private String name;

    @Nullable
    private String description;

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @ManyToOne
    private Project project;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
