package ru.baulina.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.model.Task;
import ru.baulina.tm.model.User;

import javax.servlet.http.HttpSession;

@Controller
public class TaskController {

    private final ITaskService taskService;

    @Autowired
    public TaskController(
            @NotNull final ITaskService taskService
    ) {
        this.taskService = taskService;
    }

    @GetMapping("/task/create")
    public String create(@NotNull HttpSession session) {
        @NotNull final User user = (User) session.getAttribute("user");
        taskService.create(user, "New Task " + System.currentTimeMillis());
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") Long id,
            @NotNull HttpSession session
    ) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        final Task task = taskService.findOneById(userId, id);
        return new ModelAndView("task/task-edit", "task", task);
    }

    @PostMapping("/task/edit")
    public String edit(
            @ModelAttribute("task") Task task,
            @NotNull HttpSession session
    ) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        final Long id = task.getId();
        final String name = task.getName();
        final String description = task.getDescription();
        taskService.updateTaskById(userId, id, name, description);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") Long id,
            @NotNull HttpSession session
    ) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        taskService.removeOneById(userId, id);
        return "redirect:/tasks";
    }

}
