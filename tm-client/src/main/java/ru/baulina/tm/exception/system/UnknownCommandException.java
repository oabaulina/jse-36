package ru.baulina.tm.exception.system;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException() {
        super("Error! Command not exist...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Command " + command + " not exist...");
    }

}
