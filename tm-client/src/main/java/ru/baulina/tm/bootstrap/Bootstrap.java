package ru.baulina.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class Bootstrap {

    @Autowired
    private final ApplicationEventPublisher publisher;

    @Autowired
    public Bootstrap(@NotNull final ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public void run(final String[] args) throws Exception {
        System.out.println("** Welcome to task manager **");
        System.out.println();
        try {
            if (parseArgs(args)) System.exit(0);
        } catch (Exception e) {
            logError(e);
            System.exit(0);
        }
        process();
    }

    private void process() {
        while (true) {
            try {
                synchronized(TerminalUtil.class) {
                    parseCommand(TerminalUtil.nextLine());
                }
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        publishConsoleEvent(arg);
        return true;
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        publishConsoleEvent(cmd);
    }

    private void publishConsoleEvent(@NotNull final String cmd) {
        publisher.publishEvent(new ConsoleEvent(this, cmd));
    }

}
