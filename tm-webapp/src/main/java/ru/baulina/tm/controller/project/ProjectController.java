package ru.baulina.tm.controller.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.model.Project;
import ru.baulina.tm.model.User;

import javax.servlet.http.HttpSession;

@Controller
public class ProjectController {

    private final IProjectService projectService;

    @Autowired
    public ProjectController(
            @NotNull final IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @GetMapping("/project/create")
    public String create(@NotNull HttpSession session) {
        @NotNull final User user = (User) session.getAttribute("user");
        projectService.create(user,"New Project " + System.currentTimeMillis());
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") Long id,
            @NotNull HttpSession session
    ) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        final Project project = projectService.findOneById(userId, id);
        return new ModelAndView("project/project-edit", "project", project);
    }

    @PostMapping("/project/edit")
    public String edit(
            @ModelAttribute("project") Project project,
            @NotNull HttpSession session
    ) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        final Long id = project.getId();
        final String name = project.getName();
        final String description = project.getDescription();
        projectService.updateProjectById(userId, id, name, description);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") Long id,
            @NotNull HttpSession session
    ) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        projectService.removeOneById(userId, id);
        return "redirect:/projects";
    }

}
