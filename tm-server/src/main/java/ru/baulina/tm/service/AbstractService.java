package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.baulina.tm.repository.IRepository;
import ru.baulina.tm.api.service.IService;
import ru.baulina.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public void load(@Nullable List<E> entities) {
        repository.deleteAll();
        if (entities != null) entities.forEach(repository::save);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Long count() {
        return repository.count();
    }

}
