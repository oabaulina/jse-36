package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.baulina.tm.api.endpoint.IUserEndpoint;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    public UserDTO createUser(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        sessionService.validate(session);
        @Nullable final User user = userService.create(login, password);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public UserDTO createUserWithEmail(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        sessionService.validate(session);
        @Nullable final User user = userService.create(login, password, email);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public UserDTO createUserWithRole(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "role", partName = "role") final Role role
    ) {
        sessionService.validate(session);
        @Nullable final User user = userService.create(login, password, role);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public void changeUserPassword(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "passwordOld", partName = "passwordOld") final String passwordOld,
            @WebParam(name = "passwordNew", partName = "passwordNew") final String passwordNew,
            @WebParam(name = "userId", partName = "userId") final Long userId
    ) {
        sessionService.validate(session);
        userService.changePassword(passwordOld, passwordNew, userId);
    }

    @Override
    @WebMethod
    public void profileOfUserChange(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "email", partName = "email") final String email,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "LastName", partName = "LastName") final String LastName,
            @WebParam(name = "userId", partName = "userId") final Long userId
    ) {
        sessionService.validate(session);
        userService.changeUser(email, firstName, LastName, userId);
    }

}
