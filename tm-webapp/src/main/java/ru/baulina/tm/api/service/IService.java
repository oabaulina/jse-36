package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    @Nullable
    List<E> findAll();

    void load(@Nullable final List<E> entities);

    @Nullable Long count();

}
