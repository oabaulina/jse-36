package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.dto.ProjectDTO;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    List<ProjectDTO> getProjectList();

    @WebMethod
    void loadProjects(
            @WebParam(name = "entities", partName = "entities") List<Project> projects
    );

    @WebMethod
    ProjectDTO createProject(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    ProjectDTO createProjectWithDescription(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "project", partName = "project") Project project
    );

    @WebMethod
    List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void clearProjects(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    ProjectDTO findOneProjectById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    ProjectDTO findOneProjectByName(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeOneProjectById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    void removeOneProjectByName(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void updateProjectById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

}
