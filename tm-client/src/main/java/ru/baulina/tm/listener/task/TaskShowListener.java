package ru.baulina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.event.ConsoleEvent;

import java.util.List;

@Component
public class TaskShowListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@taskShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LIST TASKS]");
        @Nullable final SessionDTO session = getSession();
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasks(session);
        int index = 1;
        for (TaskDTO task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
