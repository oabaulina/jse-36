package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class UserLockListener extends AbstractUserListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String name() {
        return "locked";
    }

    @NotNull
    @Override
    public String description() {
        return "Locked user.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@userLockListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOCKED_USER]");
        @NotNull final String login;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER LOGIN:");
            login = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        adminUserEndpoint.lockUserLogin(session, login);
        System.out.println("[OK]");
        System.out.println();
    }

}
