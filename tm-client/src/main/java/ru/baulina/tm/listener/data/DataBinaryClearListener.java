package ru.baulina.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.event.ConsoleEvent;

@Component
public class DataBinaryClearListener extends AbstractDataListener {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete bin data file.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@dataBinaryClearListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA BINARY DELETE]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataBinaryClear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
