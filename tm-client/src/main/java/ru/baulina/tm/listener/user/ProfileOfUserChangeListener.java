package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class ProfileOfUserChangeListener extends AbstractUserListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "change-profiler-of-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user's profiler.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@profileOfUserChangeListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CHANGE_PROFILE_OF_USER]");
        @Nullable final String newEmail;
        @Nullable final String newFestName;
        @Nullable final String newLastName;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER E-MAIL: ");
            newEmail = TerminalUtil.nextLine();
            System.out.println("ENTER FEST NAME: ");
            newFestName = TerminalUtil.nextLine();
            System.out.println("ENTER LAST NAME: ");
            newLastName = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        userEndpoint.profileOfUserChange(
                session, newEmail, newFestName, newLastName, session.getUserId()
        );
        System.out.println("[OK]");
        System.out.println();
    }

}
