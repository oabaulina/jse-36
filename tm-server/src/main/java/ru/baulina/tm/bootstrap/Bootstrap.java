package ru.baulina.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.endpoint.AbstractEndpoint;
import ru.baulina.tm.enumerated.Role;

import javax.xml.ws.Endpoint;
import java.util.List;

@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    private ApplicationContext applicationContext;

    @Nullable
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private List<? extends AbstractEndpoint> endpointList;

    @SneakyThrows
    private void registryEndpoints(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        assert propertyService != null;
        final String host = propertyService.getServiceHost();
        final Integer PORT = propertyService.getServicePort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + PORT + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoints() {
        for (@Nullable final AbstractEndpoint endpoint : endpointList) {
            registryEndpoints(endpoint);
        }
    }

    private void initUsers()  {
        assert userService != null;
        if (userService.findByLogin("admin") == null) {
            userService.create("test", "test", "test@test.ru");
        }
        if (userService.findByLogin("test") == null) {
            userService.create("admin", "admin", Role.ADMIN);
        }
    }

    public void run(final String[] args) throws Exception {
        initEndpoints();
        initUsers();
    }

}
