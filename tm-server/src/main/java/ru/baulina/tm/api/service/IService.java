package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    List<E> findAll();

    void load(@Nullable final List<E> entities);

    @Nullable Long count();

}
