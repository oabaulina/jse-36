package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CLEAR PROJECT]");
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.clearProjects(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
