package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.listener.AbstractListener;
import ru.baulina.tm.enumerated.Role;

public abstract class AbstractProjectListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

}
